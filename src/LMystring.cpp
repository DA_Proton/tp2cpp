#include "LMystring.h"
#include <iostream>

using namespace std;

//prgm liste
void insertionfin(plist * tete,int elt)
{
    plist aux,p;
    aux=(plist)malloc(sizeof(liste));
    aux->info=elt,
    aux->svt=NULL;
    if(*tete==NULL)
        *tete=aux;
    else
    {
        p=*tete;
        while(p->svt!=NULL)
            p=p->svt;
        p->svt=aux;
    }
}

void suppression(plist*tete,char elt)
{
    plist prec,p;
    if(*tete!=NULL)
    {
        p=*tete;
        while((p!=NULL)&&(p->info!=elt))
        {
            prec=p;
            p=p->svt;
        }
        if(p!=NULL)
        {
            if(p==*tete)
            {
                *tete=(*tete)->svt;
                free(p);
            }
            else
            {
                prec->svt=p->svt;
                free(p);
            }
        }
    }
}
//fin prgm liste

LMystring::LMystring()
{
    tete = NULL;
    stat = new int [26];
    maj();
}

LMystring::LMystring(const char* pch)
{
    //plist p;
    int i;
    for(tete = NULL, i=0; pch[i]!='\0';i++) insertionfin(&tete, pch[i]); //prgm liste
    stat = new int[26];
    maj();
}

LMystring::LMystring(int pn, char c)
{
    int i;
    for(tete = NULL, i=1; i<=pn;i++) insertionfin(&tete,c); //prgm liste
    stat = new int[26];
    maj();
}

LMystring::~LMystring()
{
    if(tete!=NULL)
    {
        plist p;
        while(tete!=NULL) //desalloc liste
        {
            p=tete->svt;
            free(tete);
            tete=p;
        }
    }
    if(stat!=NULL) delete stat;
}

LMystring::LMystring(const LMystring& str) //ctor recopie
{
    plist p;
    for(p=str.tete, tete=NULL; p!= NULL; p=p->svt) insertionfin(&tete, p->info); //prgm liste
    stat = new int[26];
    maj();
}

LMystring&LMystring::operator=(const LMystring& str)
{
    plist p;
    if(this!=&str)
    {
        this->~LMystring();
        for(p=str.tete, tete = NULL; p!=NULL;p=p->svt) insertionfin(&tete, p->info); //prgm liste
        stat = new int[26];
        maj();
    }
    return *this;
}

void LMystring::suppr(char elt)
{
    suppression(&tete, elt);
    maj();
}

void LMystring::Mytoupper()
{
    plist p;
    for(p=tete; p!=NULL; p=p->svt) p->info = toupper(p->info);
}

void LMystring::maj()
{
    plist p; char c; int i;
    spe = 0; n=0;
    for (int i=0; i<26; i++) stat[i] =0;

    for(p=tete,i=0; p!=NULL; p=p->svt, i++)
    {
        c= tolower(p->info);
        if((c >='a') && (c<='z')) stat[c-'a']++;
        else spe++;
    }
    n=i;
}

void LMystring::affiche()
{
    cout << "chaine= ";
    for(plist p= tete; p!=NULL; p=p->svt) cout << p->info;
    cout << endl << "n= " << n << endl << "spe= " << spe << endl;
    for(int i =0; i<26; i++) if(stat[i]!=0) cout << (char)(i+'a') << " = " << stat[i] << endl;
}
